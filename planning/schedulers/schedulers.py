from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import DjangoJobStore, register_events
from django.utils import timezone
from django_apscheduler.models import DjangoJobExecution
import sys

from planning.csv_imports import import_from_csv


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_jobstore(DjangoJobStore(), "default")
    scheduler.add_job(import_from_csv, 'interval', minutes=1, name='import_from_csv', jobstore='default')
    # scheduler.add_job(import_from_csv, 'interval', minutes=3, name='import_from_csv', jobstore='default')
    register_events(scheduler)
    scheduler.start()
    print("Scheduler started...", file=sys.stdout)