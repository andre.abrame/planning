import datetime
from django import template

from planning.models import Fabrication, Pesee
register = template.Library()

@register.filter
def product(value, arg):
    return value*arg

@register.filter
def divide(value, arg):
    return value//arg

@register.filter
def subtract(value, arg):
    return value-arg

@register.filter
def duration_to_pixel(value):
    print(value)
    return float(value)*31.25


@register.filter
def date_diff(value, arg): 
    print("date_debut: " + value.__str__())
    print("base: " + arg.__str__())
    return (value - arg).total_seconds()/3600

@register.filter
def status_pesee(value):
    return Pesee.Status(value).name

@register.filter
def status_fabrication(value):
    return Fabrication.Status(value).name

@register.filter
def status_pesee_to_percentage(value):
    if (value == Pesee.Status.TERMINE):
        return 100
    elif (value == Pesee.Status.EN_COURS):
        return 50
    else:
        return 0
    
@register.filter
def status_fabrication_to_percentage(value):
    if (value == Fabrication.Status.TERMINE):
        return 100
    elif (value == Fabrication.Status.EC_PLUS):
        return 66
    elif (value == Fabrication.Status.EC_MOINS):
        return 33
    else:
        return 0
    
@register.filter
def equipe_time(date: datetime.datetime, equipe: str):
    if (equipe == 'nuit'):
        date = date - datetime.timedelta(days=1)
        date = date.replace(hour = 21, minute = 0, second = 0)
    elif (equipe == 'matin'):
        date = date.replace(hour = 5, minute = 0, second = 0)
    elif (equipe == 'apres-midi'):
        date = date.replace(hour = 13, minute = 0, second = 0)
    return date.isoformat()

@register.filter
def next_week(value: list[datetime.datetime]):
    year = value[0].isocalendar().year
    week = value[0].isocalendar().week
    last_week_of_year = datetime.date(year, 12, 28).isocalendar().week
    if (week == last_week_of_year):
        return "year={}&week={}".format(year+1, 1)
    else:
        return "year={}&week={}".format(year, week+1)

@register.filter
def previous_week(value: list[datetime.datetime]):
    year = value[0].isocalendar().year
    week = value[0].isocalendar().week
    if (week == 1):
        last_week_of_previous_year = datetime.date(year-1, 12, 28).isocalendar().week
        return "year={}&week={}".format(year-1, last_week_of_previous_year)
    else:
        return "year={}&week={}".format(year, week-1)
