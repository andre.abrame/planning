from django.contrib import admin

from planning.models import Critere, Fabrication, Formule, Machine, MatierePremiere, OrdreFabrication, Pesee

admin.site.register(Critere)
admin.site.register(Fabrication)
admin.site.register(Formule)
admin.site.register(Machine)
admin.site.register(MatierePremiere)
admin.site.register(OrdreFabrication)
admin.site.register(Pesee)
