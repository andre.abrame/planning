from django.db import models


class Formule(models.Model):
    code_vrac = models.CharField(max_length=255, unique=True)
    designation = models.CharField(max_length=255)
    def __str__(self) -> str:
        return 'Formule {} - {}'.format(self.code_vrac, self.designation)

class Machine(models.Model):
    nom = models.CharField(max_length=255, unique=True)
    def __str__(self) -> str:
        return self.nom

# class ParametreMachine(models.Model):
#     nom = models.CharField(max_length=255)
#     valeurs_possibles = models.


class Fabrication(models.Model):

    class Status(models.IntegerChoices):
        NON_DEMARRE = 0
        EC_MOINS = 1
        EC_PLUS = 2
        TERMINE = 3

    date_debut = models.DateTimeField()
    informations = models.TextField()
    status = models.SmallIntegerField(choices=Status.choices, default=Status.NON_DEMARRE)
    def __str__(self) -> str:
        return 'Fabrication {} - {}'.format(self.date_debut, self.informations)


class Pesee(models.Model):

    class Status(models.IntegerChoices):
        NON_DEMARRE = 0
        OF_LANCE = 1
        OT_CREE = 2
        OF_PREPARE = 3
        EN_ATTENTE = 4
        EN_COURS = 5
        TERMINE = 10

    status = models.SmallIntegerField(choices=Status.choices, default=Status.NON_DEMARRE)
    numero_ot = models.BigIntegerField(unique=True, null=True)
    date_debut = models.DateTimeField()


class OrdreFabrication(models.Model):

    class Type(models.IntegerChoices):
        EXISTANT = 0
        FUTURE = 1
    numero_of = models.BigIntegerField(unique=True, null=True)
    numero_lot = models.CharField(max_length=255, blank=True)
    temps_pesee = models.DecimalField(decimal_places=1, max_digits=4, null=True)
    temps_fabrication = models.DecimalField(decimal_places=1, max_digits=4, null=True)
    formule = models.ForeignKey(Formule, on_delete=models.CASCADE)
    machine = models.ForeignKey(Machine, on_delete=models.CASCADE, null=True, blank=True, related_name='ordres_fabrications')
    pesee = models.ForeignKey(Pesee, on_delete=models.CASCADE, null=True, blank=True, related_name='ordre_fabrication')
    fabrication = models.ForeignKey(Fabrication, on_delete=models.SET_NULL, null=True, blank=True, related_name='ordre_fabrication')
    type = models.SmallIntegerField(choices=Type.choices)
    date_theorique = models.DateTimeField(null=True)


class Critere(models.Model):
    code_critere = models.CharField(max_length=255, unique=True)
    consigne = models.TextField()
    lien = models.CharField(max_length=255)


class MatierePremiere(models.Model):
    code_mp = models.CharField(max_length=255, unique=True)
    designation = models.CharField(max_length=255)
    formules = models.ManyToManyField(Formule, related_name='matieres_premieres')
    criteres = models.ManyToManyField(Critere, related_name='matieres_premieres')