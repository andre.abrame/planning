from datetime import datetime, timedelta, timezone
from django.http import HttpResponse
from django.shortcuts import redirect, render
from planning.forms import FabricationForm, OrdreFabricationFutureForm

from planning.models import Fabrication, Machine, OrdreFabrication, Pesee

def fabrication(request):
    week = request.GET.get('week')
    year = request.GET.get('year')
    if (week and year):
        base = datetime.fromisocalendar(int(year), int(week), 1)
    else:
        current_date = datetime.now(timezone.utc)
        base = datetime.fromisocalendar(current_date.year, current_date.isocalendar().week, 1)
    base = base.replace(tzinfo=timezone.utc)
    dates = [base + timedelta(days=x) for x in range(7)]
    base = base - timedelta(days=1)
    base = base.replace(hour = 21, minute = 0, second = 0)
    machines = Machine.objects.all()
    return render(request, 'fabrication.html', {
        'machines': machines,
        'dates': dates,
        'base_date': base,
    })

def fabrication_update_status(request):
    # retrouver la fabrication
    id_fabrication = request.GET.get('idFabrication')
    fabrication = Fabrication.objects.get(id=id_fabrication)
    # retrouver le nouveau le status
    nouveau_status = request.GET.get('status')
    fabrication.status = Fabrication.Status[nouveau_status]
    # mettre à jour la DB
    fabrication.save()
    return redirect('/fabrication')

def new_fabrication(request):
    if (request.method == 'GET'):
        if (request.GET.get('machine_id')):
            machine = Machine.objects.get(id=request.GET.get('machine_id'))
            fabrication_form = FabricationForm(machine=machine)
        else:
            fabrication_form = FabricationForm()
        if (request.GET.get('date_debut')):
            fabrication_form.fields['date_debut'].initial = datetime.fromisoformat(request.GET.get('date_debut'))
        ordre_fabrication_form = OrdreFabricationFutureForm()
        if (request.GET.get('machine_id')):
            ordre_fabrication_form.fields['machine'].initial = Machine.objects.get(id=request.GET.get('machine_id'))
        return render(request, 'new-fabrication.html', {
            'fabrication_form': fabrication_form,
            'ordre_fabrication_form': ordre_fabrication_form
        })
    else:
        fabrication_form = FabricationForm(request.POST)
        ordre_fabrication_form = OrdreFabricationFutureForm(request.POST)
        if (fabrication_form.is_valid() and ordre_fabrication_form.is_valid()):
            of_id = fabrication_form.cleaned_data.pop('ordre_fabrication')
            # creation fabrication
            fabrication = Fabrication(**fabrication_form.cleaned_data)
            fabrication.status = Fabrication.Status.NON_DEMARRE
            fabrication.save()
            # creation pesee
            if (fabrication.date_debut.weekday == 1):
                date_debut_pesee = fabrication.date_debut - timedelta(days=4)
            elif (fabrication.date_debut.weekday == 2):
                date_debut_pesee = fabrication.date_debut - timedelta(days=3)
            else:
                date_debut_pesee = fabrication.date_debut - timedelta(days=2)
            pesee = Pesee(date_debut=date_debut_pesee, status=Pesee.Status.OF_LANCE)
            pesee.save()
            # mise a jour OF
            if (of_id):
                of = OrdreFabrication.objects.get(id=of_id.id)
            else:
                of = OrdreFabrication(**ordre_fabrication_form.cleaned_data)
                of.type = OrdreFabrication.Type.FUTURE
            of.fabrication = fabrication
            of.pesee = pesee
            of.save()
            return redirect('/fabrication')
        return render(request, 'new-fabrication.html', {
            'form': form
        })

def delete_fabrication(request, id):
    fabrication = Fabrication.objects.get(id=id)
    fabrication.delete()
    return redirect('/fabrication')




def pesee(request):
    return HttpResponse('Pesees')