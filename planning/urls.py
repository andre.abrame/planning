
from django.urls import path
from planning.views import delete_fabrication, fabrication, fabrication_update_status, new_fabrication, pesee
from django.views.generic.base import RedirectView


urlpatterns = [
    path('fabrication/update/status', fabrication_update_status),
    path('fabrication/new', new_fabrication),
    path('fabrication/<int:id>/delete', delete_fabrication),
    path('fabrication', fabrication),
    path('pesee', pesee),
    path('', RedirectView.as_view(url='/fabrication'))
]