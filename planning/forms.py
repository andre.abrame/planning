from collections.abc import Mapping
from typing import Any
from django import forms
from django.forms.utils import ErrorList

from planning.models import Fabrication, Formule, Machine, OrdreFabrication


class FabricationForm(forms.Form):
    date_debut = forms.DateTimeField(label="Date début", widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    ordre_fabrication = forms.ModelChoiceField(OrdreFabrication.objects.filter(fabrication=None), required=False)
    informations = forms.CharField(label="Informations", required=False)
    status = forms.ChoiceField(required=False, initial=0, choices=Fabrication.Status.choices)

    def __init__(self, *args, **kwargs) -> None:
        try:
            machine = kwargs.pop("machine")
        except KeyError:
            machine = None
        super().__init__(*args, **kwargs)
        if (machine):
            self.fields['ordre_fabrication'].queryset = OrdreFabrication.objects.filter(fabrication=None).filter(machine=machine)


class OrdreFabricationFutureForm(forms.Form):
    formule = forms.ModelChoiceField(Formule.objects.all(), required=False)
    machine = forms.ModelChoiceField(Machine.objects.all(), required=False)
