import csv
from datetime import datetime
import os
from planning.models import Formule, Machine, OrdreFabrication
from planningjournalierfabrication.settings import BASE_DIR

def import_from_csv():
    import_of_from_csv()

def import_of_from_csv():
    print("IMPORT !!!!!!!!!!!!!!!!!!")
    with open(os.path.join(BASE_DIR, 'ImportOFPlanning.csv'), newline='') as csvfile:
        next(csvfile)
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in reader:
            # machine
            try:
                machine = Machine.objects.get(nom = row[4])
            except Machine.DoesNotExist:
                machine = Machine(nom=row[4])
                machine.save()
            # formule
            try:
                formule = Formule.objects.get(code_vrac=row[1])
            except Formule.DoesNotExist:
                formule = Formule(code_vrac=row[1], designation=row[2])
                formule.save()
            # of
            date_theorique_as_list = row[7].split("/");
            day = date_theorique_as_list[0]
            month = date_theorique_as_list[1]
            year = date_theorique_as_list[2]
            try:
                of = OrdreFabrication.objects.get(numero_of=row[0])
                of.numero_lot=row[3]
                of.temps_pesee=float(row[5])
                of.temps_fabrication=float(row[6])
                # of.date_theorique=datetime(year=year, month=month, day=day)
                of.machine=machine
                of.formule=formule
                of.type=OrdreFabrication.Type.EXISTANT
                of.save()
            except OrdreFabrication.DoesNotExist:
                of = OrdreFabrication(numero_of=int(row[0]), numero_lot=row[3], temps_pesee=float(row[5]), temps_fabrication=float(row[6]), machine=machine, formule=formule, type=OrdreFabrication.Type.EXISTANT)
                of.save()
